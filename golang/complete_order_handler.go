package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

var createShipmentURL = os.Getenv("URL") + "/shipping/shipments"

// curl --location --request POST 'https://partner.example.com/payment/ipn' \
// --header 'Content-Type: application/json' \
// --header 'X-Tiniapp-Client-Id: 8GXqhWDK3EppMwf8IyQU1GHgfq2TPADe' \
// --header 'X-Tiniapp-Signature: a9c279edaa7845a88c63eaf7c87f89dceebbc96dbed5e1a2fc9c5ffad110ad00' \
// --header 'X-Tiniapp-Timestamp: 1624552050667' \
// --data-raw '{
//   "order": {
//     "id": "87452048840261656",
//     "status": "canceled",
//     "grand_total": 50000
//   },
//   "message_type": "notification",
//   "message_id": "87461805848264725",
//   "message_created_at": "2021-06-22T23:27:29+07:00"
// }'

type TikiCreateShipmentRequest struct {
	OrderID     string         `json:"order_id" signed:"order_id"`
	PartnerCode string         `json:"partner_code" signed:"partner_code"`
	ServiceCode string         `json:"service_code" signed:"service_code"`
	Instruction string         `json:"instruction" signed:"instruction"`
	Items       []ShippingItem `json:"items" signed:"items"`
	Origin      Origin         `json:"origin" signed:"origin"`
	Destination Destination    `json:"destination" signed:"destination"`
}

type orderIPNReq struct {
	MessageID        string          `json:"message_id"`
	MessageType      string          `json:"message_type"`
	MessageCreatedAt time.Time       `json:"message_created_at"`
	Order            IPNOrderPayload `json:"order"`
}

type IPNOrderPayload struct {
	ID         string `json:"id"`
	Status     string `json:"status"`
	GrandTotal int64  `json:"grand_total"`
}

func completeOrderHandler(c *gin.Context) {
	var req orderIPNReq

	// TODO VERIFY TIKI SIGNATURE

	// validate input
	if err := c.ShouldBindJSON(&req); err != nil {
		fmt.Println("Error: ", err)
		c.JSON(400, map[string]string{
			"code":  "400",
			"error": "invalid request",
		})
		return
	}

	if err := db.Model(&Order{}).Where("tiki_order_id = ?", req.Order.ID).
		Update("status", req.Order.Status).Error; err != nil {
		fmt.Println("Error: ", err)
		c.JSON(500, map[string]string{
			"code":  "500",
			"error": "invalid request",
		})
		return
	}

	if req.Order.Status == "success" {
		var order Order
		tikiOrderID, _ := strconv.ParseInt(req.Order.ID, 10, 64)

		if err := db.Model(&Order{}).
			Where("tiki_order_id = ?", tikiOrderID).First(&order).Error; err != nil {
			fmt.Println("Error: ", err)
			c.JSON(500, map[string]string{
				"code":  "500",
				"error": "invalid request",
			})
			return
		}

		var items []Item
		if err := db.Model(&Item{}).
			Where("order_id = ?", order.ID).Find(&items).Error; err != nil {
			fmt.Println("Error: ", err)
			c.JSON(500, map[string]string{
				"code":  "500",
				"error": "invalid request",
			})
			return
		}

		var origin Address
		if err := db.Model(&Address{}).
			Where("order_id = ? AND kind = 'origin'", order.ID).First(&origin).Error; err != nil {
			fmt.Println("Error: ", err)
			c.JSON(500, map[string]string{
				"code":  "500",
				"error": "invalid request",
			})
			return
		}

		var destination Address
		if err := db.Model(&Address{}).
			Where("order_id = ? AND kind = 'destination'", order.ID).First(&destination).Error; err != nil {
			fmt.Println("Error: ", err)
			c.JSON(500, map[string]string{
				"code":  "500",
				"error": "invalid request",
			})
			return
		}

		var shippingItems []ShippingItem
		for _, item := range items {
			shippingItems = append(shippingItems, ShippingItem{
				Name:        item.Name,
				Description: item.Name,
				Quantity:    item.Quantity,
				Price:       float64(item.Price),
				Weight:      float64(item.Weight),
				Dimension: Dimension{
					Height: float64(item.Height),
					Width:  float64(item.Width),
					Depth:  float64(item.Depth),
				},
			})
		}

		shipmentReq := TikiCreateShipmentRequest{
			OrderID:     fmt.Sprintf("%d", order.TikiOrderID),
			PartnerCode: order.ShippingPartnerCode,
			ServiceCode: order.ShippingServiceCode,
			Items:       shippingItems,
			Origin: Origin{
				Street:       "285 CMT8",
				WardName:     "Phường 12",
				DistrictName: "Quận 10",
				ProvinceName: "Hồ Chí Minh",
				WardCode:     "VN039007012",
				Contact: &Contact{
					FirstName: "TiniApp",
					LastName:  "Store",
					Email:     "tini-app@tiki.vn",
					Phone:     "0958364543",
				},
				Coordinates: Coordinates{
					Latitude:  10.779693436530149,
					Longitude: 106.67971686137946,
				},
			},
			Destination: Destination{
				Street:       destination.Street,
				ProvinceName: destination.Province,
				DistrictName: destination.District,
				WardName:     destination.Ward,
				WardCode:     destination.WardCode,
				Contact: &Contact{
					FirstName: destination.Name,
					LastName:  "",
					Email:     "customer@tiki.vn",
					Phone:     destination.Phone,
				},
				Coordinates: Coordinates{
					Latitude:  10.779693436530149,
					Longitude: 106.67971686137946,
				},
			},
		}

		// call platform API
		body, _ := json.Marshal(shipmentReq)
		respBytes, err := makeSecureRequest(createShipmentURL, http.MethodPost, body)
		if err != nil {
			c.JSON(400, map[string]string{"code": "400", "error": err.Error()})
			return
		}

		fmt.Println("Tiki Response: ", string(respBytes))
	}

	fmt.Printf("Input: %+v\n", req)
	c.JSON(200, map[string]string{
		"state": "updated",
	})
}
