package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

var orderURL = os.Getenv("URL") + "/order"

// CREATE TABLE items(
//   id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
//   order_id INT UNSIGNED NOT NULL,
//   name TEXT NOT NULL,
//   descr TEXT NOT NULL,
//   quantity INT UNSIGNED NOT NULL,
//   price INT NOT NULL,
//   weight INT NOT NULL,
//   width INT NOT NULL,
//   height INT NOT NULL,
//   depth INT NOT NULL,
//   created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
//   updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
// ) ENGINE=InnoDB;

type Item struct {
	ID        int       `json:"id"`
	OrderID   int       `json:"order_id"`
	Name      string    `json:"name"`
	Descr     string    `json:"descr"`
	Quantity  int       `json:"quantity"`
	Price     int       `json:"price"`
	Weight    int       `json:"weight"`
	Height    int       `json:"height"`
	Width     int       `json:"width"`
	Depth     int       `json:"depth"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// CREATE TABLE orders(
//   id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
// 	tiki_order_id INT,
//   user_id INT UNSIGNED NOT NULL,
//   user_name TEXT NOT NULL,
//   sub_total INT UNSIGNED NOT NULL,
//   shipping INT UNSIGNED NOT NULL,
//   total INT UNSIGNED NOT NULL,
//   created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
//   updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
// ) ENGINE=InnoDB;

type Order struct {
	ID                  int       `json:"id"`
	TikiOrderID         int       `json:"tiki_order_id"`
	UserID              int       `json:"user_id"`
	UserName            string    `json:"user_name"`
	SubTotal            int       `json:"sub_total"`
	Shipping            int       `json:"shipping"`
	ShippingPartnerCode string    `json:"shipping_partner_code"`
	ShippingServiceCode string    `json:"shipping_service_code"`
	Total               int       `json:"total"`
	Status              string    `json:"status"`
	CreatedAt           time.Time `json:"created_at"`
	UpdatedAt           time.Time `json:"updated_at"`
}

// CREATE TABLE addresses (
//   id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
//   order_id INT NOT NULL,
//   name TEXT,
//   phone TEXT,
//   street TEXT,
//   province TEXT,
//   district TEXT,
//   ward     TEXT,
//   ward_code     TEXT,
//   kind TEXT, -- origin | destination
//   created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
//   updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
// ) ENGINE=InnoDB;

type Address struct {
	ID       int    `json:"id"`
	OrderID  int    `json:"order_id"`
	Name     string `json:"name"`
	Phone    string `json:"phone"`
	Street   string `json:"street"`
	Province string `json:"province"`
	District string `json:"district"`
	Ward     string `json:"ward"`
	WardCode string `json:"ward_code"`
	Kind     string `json:"kind"` // origin | destination
}

type newOrderInput struct {
	Order struct {
		Items []struct {
			Name     string `json:"name"`
			Descr    string `json:"descr"`
			Quantity int    `json:"quantity"`
			Price    int    `json:"price"`
			Weight   int    `json:"weight"`
			Height   int    `json:"height"`
			Width    int    `json:"width"`
			Depth    int    `json:"depth"`
		} `json:"items"`

		OriginAddress struct {
			Name     string `json:"name"`
			Phone    string `json:"phone"`
			Email    string `json:"email"`
			Street   string `json:"street"`
			Province string `json:"province"`
			District string `json:"district"`
			Ward     string `json:"ward"`
			WardCode string `json:"ward_code"`
		} `json:"origin_address"`

		ShippingAddress struct {
			Name     string `json:"name"`
			Phone    string `json:"phone"`
			Email    string `json:"email"`
			Street   string `json:"street"`
			Province string `json:"province"`
			District string `json:"district"`
			Ward     string `json:"ward"`
			WardCode string `json:"ward_code"`
		} `json:"shipping_address"`

		ShippingInfo struct {
			PartnerCode string `json:"partner_code"`
			ServiceCode string `json:"service_code"`
		} `json:"shipping_info"`

		SubTotal int `json:"sub_total"`
		Shipping int `json:"shipping"`
		Total    int `json:"total"`
	} `json:"order"`
}

func createOrderHandler(c *gin.Context) {
	var req newOrderInput

	// extract token from header
	token := c.Request.Header.Get("X-Access-Token")

	// get user info from Tiki
	userInfo, err := getUserInfo(token)
	if err != nil || userInfo.Data.CustomerID == 0 {
		c.JSON(400, map[string]string{
			"code":  "400",
			"error": fmt.Sprintf("invalid user %+v", userInfo),
		})
		return
	}
	fmt.Printf("User info: %+v\n", userInfo)

	// make sure we have valid input
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(400, map[string]string{
			"code":  "400",
			"error": "invalid request",
		})
		return
	}

	fmt.Printf("Input: %+v\n", req)

	order := Order{
		UserID:              userInfo.Data.CustomerID,
		UserName:            userInfo.Data.CustomerName,
		SubTotal:            req.Order.SubTotal,
		Shipping:            req.Order.Shipping,
		ShippingPartnerCode: req.Order.ShippingInfo.PartnerCode,
		ShippingServiceCode: req.Order.ShippingInfo.ServiceCode,
		Total:               req.Order.Total,
		Status:              "draft",
	}
	fmt.Printf("Saving order: %+v\n", order)
	if err := db.Create(&order).Error; err != nil {
		c.JSON(500, map[string]string{
			"code":  "500",
			"error": err.Error(),
		})
		return
	}

	if req.Order.OriginAddress.Name != "" {
		address := Address{
			Kind:     "origin",
			OrderID:  order.ID,
			Name:     req.Order.OriginAddress.Name,
			Phone:    req.Order.OriginAddress.Phone,
			Street:   req.Order.OriginAddress.Street,
			Province: req.Order.OriginAddress.Province,
			District: req.Order.OriginAddress.District,
			Ward:     req.Order.OriginAddress.Ward,
			WardCode: req.Order.OriginAddress.WardCode,
		}
		if err := db.Create(&address).Error; err != nil {
			fmt.Println("Error: ", err.Error())
			c.JSON(500, map[string]string{
				"code":  "500",
				"error": err.Error(),
			})
			return
		}
	}

	if req.Order.ShippingAddress.Name != "" {
		address := Address{
			Kind:     "destination",
			OrderID:  order.ID,
			Name:     req.Order.ShippingAddress.Name,
			Phone:    req.Order.ShippingAddress.Phone,
			Street:   req.Order.ShippingAddress.Street,
			Province: req.Order.ShippingAddress.Province,
			District: req.Order.ShippingAddress.District,
			Ward:     req.Order.ShippingAddress.Ward,
			WardCode: req.Order.ShippingAddress.WardCode,
		}
		if err := db.Create(&address).Error; err != nil {
			fmt.Println("Error: ", err.Error())
			c.JSON(500, map[string]string{
				"code":  "500",
				"error": err.Error(),
			})
			return
		}
	}

	item := Item{
		OrderID:  order.ID,
		Name:     req.Order.Items[0].Name,
		Descr:    req.Order.Items[0].Descr,
		Quantity: req.Order.Items[0].Quantity,
		Price:    req.Order.Items[0].Price,
		Weight:   req.Order.Items[0].Weight,
		Height:   req.Order.Items[0].Height,
		Width:    req.Order.Items[0].Width,
		Depth:    req.Order.Items[0].Depth,
	}
	if err := db.Create(&item).Error; err != nil {
		c.JSON(500, map[string]string{
			"code":  "500",
			"error": err.Error(),
		})
		return
	}

	// create Tiki order
	itemsPayLoad := []TikiItemPayload{{
		Name:     item.Name,
		Sku:      "8715193034472",
		Quantity: item.Quantity,
		Price:    item.Price,
	}}

	tikiReq := TikiCreateOrderRequest{
		CustomerID: fmt.Sprintf("%d", order.UserID),
		Items:      itemsPayLoad,
		BillingAddress: TikiAddressPayload{
			Name:   order.UserName,
			Phone:  "0987654321",
			Street: "285 CMT8",
		},
		Extra:       "",
		ReferenceID: fmt.Sprintf("%v", order.ID),
	}

	body, _ := json.Marshal(tikiReq)
	respBytes, err := makeSecureRequest(orderURL, http.MethodPost, body)
	if err != nil {
		c.JSON(400, map[string]string{
			"code":  "400",
			"error": err.Error(),
		})
		return
	}

	fmt.Println("Tiki return", string(respBytes))
	tikiOrderResp := TikiOrderResp{}
	json.Unmarshal(respBytes, &tikiOrderResp)

	if err := db.Model(&Order{}).Where("id = ?", order.ID).
		Update("tiki_order_id", tikiOrderResp.Data.Order.ID).Error; err != nil {
		c.JSON(500, map[string]string{
			"code":  "500",
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, map[string]interface{}{
		"tiki_order": tikiOrderResp.Data.Order,
		"backend_order": map[string]interface{}{
			"id": order.ID,
		},
	})
}

type TikiOrderResp struct {
	Data struct {
		Order struct {
			ID         string `json:"id"`
			Status     string `json:"status"`
			GrandTotal int    `json:"grand_total"`
		} `json:"order"`
	} `json:"data"`
}

type TikiAddressPayload struct {
	Name   string `json:"name"`
	Phone  string `json:"phone"`
	Email  string `json:"email"`
	Street string `json:"street"`
}

type TikiItemPayload struct {
	Name     string `json:"name"`
	Sku      string `json:"sku"`
	Quantity int    `json:"quantity"`
	Price    int    `json:"price"`
	Extra    string `json:"extra"`
}

type TikiCreateOrderRequest struct {
	CustomerID      string             `json:"customer_id"`
	Items           []TikiItemPayload  `json:"items"`
	ShippingAddress TikiAddressPayload `json:"shipping_address"`
	BillingAddress  TikiAddressPayload `json:"billing_address"`
	Extra           string             `json:"extra"`
	ReferenceID     string             `json:"reference_id"`
}

type TikiOrder struct {
	ID         string `json:"id"`
	Status     string `json:"status"`
	GrandTotal int64  `json:"grand_total"`
}

type TikiOrderResponse struct {
	Data struct {
		Order Order `json:"order"`
	} `json:"data"`
}
