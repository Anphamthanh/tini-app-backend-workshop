package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

var authURL = os.Getenv("URL") + "/api/v1/oauth/auth/token"

type authInput struct {
	Code string `json:"code" binding:"required"`
}

type authRespData struct {
	TikiAccessToken string   `json:"tiki_access_token"`
	AccessToken     string   `json:"access_token"`
	RefreshToken    string   `json:"refresh_token"`
	ExpiresIn       int64    `json:"expires_in"`
	Scopes          []string `json:"scopes"`
	TokenType       string   `json:"token_type"`
	User            struct {
		MuchuUserID int64  `json:"muchu_user_id"`
		TikiUserID  int64  `json:"tiki_user_id"`
		Name        string `json:"name"`
		Phone       string `json:"phone"`
	} `json:"user"`
}

type authResp struct {
	Data struct {
		AccessToken string `json:"access_token"`
		Customer    struct {
			ID   int    `json:"id"`
			Name string `json:"name"`
		} `json:"customer"`
		ExpiresIn int `json:"expires_in"`
	} `json:"data"`
}

func authHandler(c *gin.Context) {
	var req authInput

	// make sure we have valid input
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(400, map[string]string{"code": "400", "error": "invalid request"})
		return
	}

	// call platform API
	body, _ := json.Marshal(req)
	respBytes, err := makeSecureRequest(authURL, http.MethodPost, body)
	if err != nil {
		c.JSON(400, map[string]string{"code": "400", "error": err.Error()})
		return
	}

	// process the response
	// Sample data {"data":{"access_token":"token","customer":{"id":86354,"name":"An Pham"},"expires_in":3599,"refresh_token":"","scopes":["user_profile"],"token_type":"bearer"},"error":null}
	resp := authResp{}
	if err := json.Unmarshal(respBytes, &resp); err != nil {
		c.JSON(500, map[string]string{"code": "500", "error": err.Error()})
		return
	}

	fmt.Printf("Return data: %+v\n", resp)
	// better to sign your token and return here instead of return tiki token
	c.JSON(200, map[string]interface{}{
		"data": resp.Data,
	})
}
