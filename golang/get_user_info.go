package main

import (
	"encoding/json"
	"net/http"
	"os"
)

var userInfoURL = os.Getenv("URL") + "/api/v1/oauth/me"

type userInfoReq struct {
	AccessToken string `json:"access_token"`
}

type userInfoResp struct {
	Data struct {
		CustomerID   int      `json:"customer_id"`
		CustomerName string   `json:"customer_name"`
		Scopes       []string `json:"scopes"`
	}
}

func getUserInfo(token string) (*userInfoResp, error) {
	req := &userInfoReq{AccessToken: token}
	body, _ := json.Marshal(req)
	respBody, err := makeSecureRequest(userInfoURL, http.MethodPost, body)
	if err != nil {
		return nil, err
	}

	userInfo := &userInfoResp{}
	err = json.Unmarshal(respBody, userInfo)
	if err != nil {
		return nil, err
	}

	return userInfo, nil
}
