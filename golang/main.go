package main

import (
	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var db *gorm.DB

func main() {
	var err error
	dsn := "root:change_me@tcp(localhost:3319)/miniapp_demo?parseTime=true"
	db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	router := gin.Default()
	router.GET("/ping", pingHandler)
	router.POST("/auth", authHandler)
	router.POST("/orders", createOrderHandler)
	router.POST("orders/complete", completeOrderHandler)
	router.POST("shipping/quotes", listQuoteHandler)
	router.POST("shipping/update", updateShipmentHandler)
	router.Run()
}

func pingHandler(c *gin.Context) {
	c.JSON(200, "pong")
}
