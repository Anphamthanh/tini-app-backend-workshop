package main

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"
)

type secureRequestInput struct {
	URL    string
	Method string
	Body   []byte
}

func sign(secret string, payload string) (string, error) {
	h := hmac.New(sha256.New, []byte(secret))
	_, err := h.Write([]byte(payload))
	if err != nil {
		return "", err
	}
	signature := hex.EncodeToString(h.Sum(nil))
	return signature, nil
}

func makeSecureRequest(url string, method string, body []byte) ([]byte, error) {
	// calculate timestamp
	requestTime := time.Now().UnixNano() / int64(time.Millisecond)
	timestamp := strconv.FormatInt(requestTime, 10)

	// sign the payload
	signingPayload := fmt.Sprintf("%s.%s.%s", timestamp, os.Getenv("KEY"), string(body))
	signingPayload = base64.RawURLEncoding.EncodeToString([]byte(signingPayload))
	signature, err := sign(os.Getenv("SEC"), signingPayload)
	if err != nil {
		return nil, err
	}

	// build up the request object
	httpReq, _ := http.NewRequest(method, url, bytes.NewReader(body))
	httpReq.Header.Set("Content-Type", "application/json")
	httpReq.Header.Set("X-Tiniapp-Timestamp", timestamp)
	httpReq.Header.Set("X-Tiniapp-Client-Id", os.Getenv("KEY"))
	httpReq.Header.Set("X-Tiniapp-Signature", signature)

	// init http client
	client := http.Client{}
	httpResp, err := client.Do(httpReq)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = httpResp.Body.Close()
	}()

	// read response
	responseBody, err := ioutil.ReadAll(httpResp.Body)
	if err != nil {
		return nil, err
	}

	return responseBody, nil
}
