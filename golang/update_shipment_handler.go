package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

type shipmentEvent struct {
	TrackingID string `json:"tracking_id"`
	Status     string `json:"status"`
}

func updateShipmentHandler(c *gin.Context) {
	var req shipmentEvent

	// validate input
	if err := c.ShouldBindJSON(&req); err != nil {
		fmt.Println("Error: ", err)
		c.JSON(400, map[string]string{
			"code":  "400",
			"error": "invalid request",
		})
		return
	}

	fmt.Println("Shipping Callback: ", req)
	c.JSON(200, map[string]string{
		"state": "updated",
	})
}
