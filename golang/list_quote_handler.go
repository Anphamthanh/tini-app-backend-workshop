package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

var shippingURL = os.Getenv("URL") + "/shipping/quotes"

type getQuoteReq struct {
	Items       []*ShippingItem `json:"items"`
	Origin      Origin          `json:"origin"`
	Destination Destination     `json:"destination"`
}

type ShippingItem struct {
	Name        string    `json:"name" signed:"name"`
	Description string    `json:"description" signed:"description"`
	Quantity    int       `json:"quantity" signed:"quantity"`
	Price       float64   `json:"price" signed:"price"`
	Weight      float64   `json:"weight" signed:"weight"`
	Dimension   Dimension `json:"dimension" signed:"dimension"`
}

type Dimension struct {
	Height float64 `json:"height" signed:"height"`
	Width  float64 `json:"width" signed:"width"`
	Depth  float64 `json:"depth" signed:"depth"`
}

type Origin struct {
	Street       string      `json:"street" signed:"street"`
	WardName     string      `json:"ward_name" signed:"ward_name"`
	DistrictName string      `json:"district_name" signed:"district_name"`
	ProvinceName string      `json:"province_name" signed:"province_name"`
	WardCode     string      `json:"ward_code" signed:"ward_code"`
	Contact      *Contact    `json:"contact,omitempty" signed:"contact"`
	Coordinates  Coordinates `json:"coordinates"`
}

type Destination struct {
	Street       string      `json:"street" signed:"street"`
	WardName     string      `json:"ward_name" signed:"ward_name"`
	DistrictName string      `json:"district_name" signed:"district_name"`
	ProvinceName string      `json:"province_name" signed:"province_name"`
	WardCode     string      `json:"ward_code" signed:"ward_code"`
	Contact      *Contact    `json:"contact,omitempty" signed:"contact"`
	Coordinates  Coordinates `json:"coordinates"`
}

type Contact struct {
	FirstName string `json:"first_name" signed:"first_name"`
	LastName  string `json:"last_name" signed:"last_name"`
	Email     string `json:"email" signed:"email"`
	Phone     string `json:"phone" signed:"phone"`
}

type Coordinates struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type getQuoteResp struct {
	TiniAppErrorInfo *struct {
		Code    int    `json:"code,omitempty"`
		Message string `json:"message,omitempty"`
		Reason  string `json:"reason,omitempty"`
	}
	Data struct {
		Quotes []Quote `json:"quotes"`
	} `json:"data"`
}

type Quote struct {
	EstimatedTimeline EstimatedTimeline `json:"estimated_timeline"`
	Fee               Fee               `json:"fee"`
	PartnerCode       string            `json:"partner_code"`
	Service           Service           `json:"service"`
}

type EstimatedTimeline struct {
	Dropoff string `json:"dropoff"`
	Pickup  string `json:"pickup"`
}

type Fee struct {
	Amount   float64 `json:"amount"`
	UnitCode string  `json:"unit_code"`
}

type Service struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Code string `json:"code"`
}

func listQuoteHandler(c *gin.Context) {
	var req getQuoteReq

	// security, check token
	token := c.Request.Header.Get("X-Access-Token")

	// get user info from Tiki
	userInfo, err := getUserInfo(token)
	if err != nil || userInfo.Data.CustomerID == 0 {
		c.JSON(400, map[string]string{
			"code":  "400",
			"error": fmt.Sprintf("invalid user %+v", userInfo),
		})
		return
	}
	fmt.Printf("User info: %+v\n", userInfo)

	// make sure we have valid input
	if err := c.ShouldBindJSON(&req); err != nil {
		fmt.Println("Error: ", err.Error())
		c.JSON(400, map[string]string{
			"code":  "400",
			"error": "invalid request",
		})
		return
	}

	fmt.Printf("Input: %+v\n", req)
	body, _ := json.Marshal(req)
	respBytes, err := makeSecureRequest(shippingURL, http.MethodPost, body)
	if err != nil {
		fmt.Println("Error: ", err.Error())
		c.JSON(400, map[string]string{
			"code":  "400",
			"error": "invalid request",
		})
		return
	}

	resp := getQuoteResp{}
	json.Unmarshal(respBytes, &resp)

	fmt.Printf("Tiki return: %+v\n", resp)
	c.JSON(200, map[string]interface{}{
		"quotes": resp,
	})
}
