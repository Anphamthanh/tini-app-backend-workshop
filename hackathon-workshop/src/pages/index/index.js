const backendURL = 'https://b1432a92535f.ngrok.io'
Page({
  data: {
    token: "",
    name: "",
  },
  onLoad(query) {
    my.getAuthCode({
      success: (code) => {
        my.request({
          url: `${backendURL}/auth`,
          method: 'POST',
          token: '',
          headers: {
            'Content-Type': 'application/json'
          },
          data: { code: code.authCode },
          success: (tokenResp) => {
            console.log(tokenResp.data.access_token)
            this.setData({
              token: tokenResp.data.access_token,
              name: tokenResp.data.customer.name,
            })
          },
          fail: (err) => {
            console.log(err)
          }
        })
      },
      fail: (err) => {
        console.log(err)
      }
    })
  },
  createOrder() {
    my.request({
      url: `${backendURL}/orders`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-Access-Token': this.data.token,
      },
      data: {
        order: {
          items: [{
            name: "Sách C++",
            descr: 'C++11 and C++14 is more than a matter of familiarizing yourself',
            quantity: 1,
            price: 150000,
            weight: 1, // kg
            height: Math.floor(Math.random() * 100),
            width: Math.floor(Math.random() * 100),
            depth: Math.floor(Math.random() * 100),
          }],
          shipping_address: {
            name: 'An Pham',
            street: '36 Tân Thanh',
            province: 'Hồ Chí Minh',
            district: "Quận Tân Bình",
            ward: "Phường Sơn Kỳ",
            phone: '0987654321',
            ward_code: 'VN039023006' // lấy từ Tiki
          },
          origin_address: {
            name: 'An Pham',
            street: '285 CMT8',
            province: 'Hồ Chí Minh',
            district: "Quận 10",
            ward: "Phường 14",
            phone: '0987654321',
            ward_code: 'VN039023006' // lấy từ Tiki
          },
          shipping_info: {
            partner_code: 'AHM',
            service_code: 'SGN-BIKE'
          },
          sub_total: 150000,
          shipping: 50000,
          total: 200000,
        }
      },
      success: (orderData) => {
        console.log(orderData)
        this.setData({
          tikiOrderID: orderData.tiki_order.id,
          orderID: orderData.backend_order.id,
          total: orderData.tiki_order.id.grand_total,
          result: `Order created`,
        })
      },
      fail: (err) => {
        console.log(err)
      }
    })
  },
  getQuotes() {
    my.request({
      url: `${backendURL}/shipping/quotes`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-Access-Token': this.data.token,
      },
      data: {
        origin: {
          street: '285 CMT8',
          province_name: 'Hồ Chí Minh',
          district_name: "Quận 10",
          ward_name: "Phường 12",
          ward_code: "VN039007012",
          contact: {
            first_name: 'An',
            last_name: 'Pham',
            phone: '0958364543',
          },
          coordinates: {
            latitude: 10.779693436530149,
            longitude: 106.67971686137946,
          }
        },
        destination: {
          street: '36 Tân Thanh',
          province_name: 'Hồ Chí Minh',
          district_name: "Quận Tân Bình",
          ward_name: "Phường Sơn Kỳ",
          ward_code: "VN039023006",
          contact: {
            first_name: 'An',
            last_name: 'Pham',
            phone: '0958364543',
          },
          coordinates: {
            latitude: 10.8013905611673,
            longitude: 106.61454563971664,
          }
        },
        items: [{
          name: "Sách C++",
          description: 'C++11 and C++14 is more than a matter of familiarizing yourself',
          quantity: 1,
          price: 150000,
          weight: 1, // kg
          dimension: {
            height: Math.floor(Math.random() * 100),
            width: Math.floor(Math.random() * 100),
            depth: Math.floor(Math.random() * 100),
          },
        }],
      },
      success: (quotes) => {
        console.log(quotes.quotes.data.quotes)
        // this.setData({
        //   result: `Quotes received`
        // })
      },
      fail: (err) => {
        console.log(err)
        // my.alert({
        //   title: 'Error',
        //   content: err,
        //   buttonText: 'OK',
        // })
      },
    })
  },
  makePayment(e) {
    console.log(e.target.dataset.status)
    my.request({
      url: `${backendURL}/orders/complete`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      data: {
        "order": {
          "id": this.data.tikiOrderID,
          "status": e.target.dataset.status,
          "grand_total": this.data.total,
        },
        "message_type": "notification",
        "message_id": "87461805848264725",
        "message_created_at": "2021-07-18T23:27:29+07:00",
      },
      success: (result) => {
        console.log(result)
      },
      fail: (err) => {
        console.log(err)
      }
    })
  },
  onReady() {
  },
  onShow() {
  },
  onHide() {
  },
  onUnload() {
  }
});
